---
title: "bootstrap"
date: 2019-02-02T00:22:03-08:00
lastmod: 2019-02-02T00:22:03-08:00
draft: false
tags: ["tech", "apple"]
categories: ["tech"]
author: "james bunch"
comment: false
toc: false
---

Just over 5 years ago now I put together [bootstrap](https://github.com/fallwith/bootstrap/),
a GitHub hosted repo with content for prepping a new macOS system. I've now
given it a celebratory anniversary overhaul and wanted to share it - and more
importantly the generic concept behind it - with you. I use bootstrap to keep
lots of system and application configuration backed up, to be able to prep a
fresh computer quickly with my desired customizations, and to be able to keep
multiple machines in sync. Then there's also the usual benefits of having the
content stored in a revision control system.

Hopefully those reasons are enough to convince you to get your own
"dotfiles"* repository going if you haven't already.

_* dotfiles are so named because their names start with a period. BSD based
systems like macOS automatically hide these files from view._

My project comes with an `init` script which will take care of the following:

* Sets up a `bin` subdirectory beneath the user's home directory
* Configures some macOS system settings, such as the keyboard repeat rate
* Installs [Homebrew](https://brew.sh/) and uses it to install software
* Preps a Homebrew installed shell to be the user's default one
* Symlinks a variety of app config files in their appropriate locations

For me, this means that I get all of these things accomplished very quickly:

* Install and configure the [kitty](https://sw.kovidgoyal.net/kitty/) terminal
  emulator
* Install fonts suitable for development like [mononoki](https://madmalik.github.io/mononoki/)
* Install and configure [NeoVim](https://neovim.io/)
* Install all of my desired App Store hosted apps with [mas](https://formulae.brew.sh/formula/mas)
* Configure [ranger](https://ranger.github.io/) to preview images in the terminal
* Prep [mksh](http://www.mirbsd.org/mksh.htm) and have it be my default shell

You are welcome to use bootstrap however you'd like to, be it forking it and
tweaking it to suit your desires, borrowing a few select things from it, or
merely getting on board with the concept of having your dotfiles stored and
using a completely different approach to implementation.

There are loads of different ways to accomplish the goal of caring for your
configuration content. Apart from mine, here are some others to get you started:

* Mathias Bynens' [dotfiles](https://github.com/mathiasbynens/dotfiles) repo
* The [GNU Stow](https://www.gnu.org/software/stow/) tool
* thoughbot's [rcm](https://github.com/thoughtbot/rcm) tool
* Laurent Raufaste's [mackup](https://github.com/lra/mackup) repo

If you're feeling inspired and end up implementing something to manage your
own content or if you've already been managing your dotfiles with an approach
you'd like to share with me, please reach out via the links below and let me
know.

