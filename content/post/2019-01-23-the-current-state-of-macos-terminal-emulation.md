---
title: "The Current State of macOS Terminal Emulation"
date: 2019-01-23T00:44:23-08:00
lastmod: 2019-01-23T00:44:23-08:00
draft: false
tags: ["tech", "apple", "unix"]
categories: ["tech"]
author: "james bunch"
comment: false
toc: false
---

With macOS, the current Apple Mac computer offers an excellent unix based
environment with a wealth of powerful terminal emulators to choose from.

I've selected five to see where terminal emulation under macOS is at here in the
beginning of 2019.

First I'll zip through each and highlight what I do and don't like about each
and then I'll compare them all based on the criteria that matters most to me.


# A Quick Look At Each Emulator

## Terminal

![Terminal](/img/2019-01-23-the-current-state-of-macos-terminal-emulation/Terminal.png)

https://support.apple.com/guide/terminal/welcome/mac

Authored by Apple and included with macOS, Terminal is the incumbent. It's
widely regarded as a good bit of kit and for most users one that's completely
fine to stick with. There have even been power users (such as
[joshtronic](https://joshtronic.com/2016/03/20/switching-from-iterm2-to-terminal/))
who have written about returning to Terminal after an extended time away with
other alternatives, either because it just works or because it works better
than others.

##### Pros
* Offers a graphical configuration tool with support for multiple profiles
* Your macOS shortcuts from other apps (`cmd+shift+arrow = change tabs`) work here

##### Cons
* The only non open source choice
* Ships with the dubiously user agreement based SF Mono font
* Offers some of the fewest features and lowest performance


## Alacritty

![Alacritty](/img/2019-01-23-the-current-state-of-macos-terminal-emulation/Alacritty.png)

https://github.com/jwilm/alacritty

Alacritty is devoted to becoming the fastest terminal emulator in existence,
and the occasional [controversy](https://github.com/jwilm/alacritty/pull/798)
not withstanding, it is doing very well towards that goal. With
an open source Rust codebase, Alacritty deliberately eschews functionality in
the name of speed. Under macOS at least, the focus is working, with Alacritty
regularly demonstrating itself to be either the fastest or second fastest
(to Kitty) performer. If you're quite happy to leave the windowing, tabs, and
splits functionality up to your window manager and/or tmux and have your
terminal emulator focused on performance, Alacritty might be the one for you.

##### Pros
* Open source and cross-platform
* Fast

##### Cons
* As with a muscle car, some features have been left out by design
* Vim and tmux users must disable the background color in those apps for transparency to work (see [issue 1082](https://github.com/jwilm/alacritty/issues/1082))



## Hyper

![Hyper](/img/2019-01-23-the-current-state-of-macos-terminal-emulation/Hyper.png)

https://hyper.is

How you get on with Hyper and other Electron based terminal emulators like it
largely depends on how you view the use of JavaScript apps on the desktop. If
you're happily using apps like Atom or Slack, you'll likely enjoy using Hyper
as well and enjoy feeling unencumbered by the limitations typically imposed by
traditional emulators while you delight in showing off your fully animated
effects and other features on offer. If you're no fan of
JavaScript's performance or its demand on your system's resources and you'd
happily sacrifice some visual effects in the name of gaining speed, you might
not get on any better with Hyper than you have with other JavaScript apps.

##### Pros
* Effects you'll only find with "web technologies"
* Open source and cross-platform
* Offers plugins and themes

##### Cons
* Lacks support for 24-bit color, GPU support, and CLI based image rendering
* Can't map the sending of hex codes to key combinations
* One of the poorest performers if not the poorest


## iTerm2

![iTerm2](/img/2019-01-23-the-current-state-of-macos-terminal-emulation/iTerm2.png)

https://iterm2.com

The longest running alternative to Terminal on the list, iTerm2's macOS only
focus and extensive history of constant improvements and additions have
resulted in quite a rich list of
[features](https://www.iterm2.com/features.html). The entirety of the
extensive work done on iTerm2 has been done in the open with excellent
engagement with the open source community. The gold standard for macOS terminal
emulators, every other project must deal with a slew of "in iTerm2 I'm used
to doing x, how can I do the same with your app?" issues.

##### Pros
* Open source
* Proven track record for project stewardship and community interaction
* Offers the most trimmings around the emulator itself with lots of GUI app functionality

##### Cons
* Odd TouchBar behavior providing rapidly changing options while a stdout stream is being rendered
* GPU performance is limited to opaque windows and trails behind Kitty and Alacritty
* Configuration is in macOS .plist format


## Kitty

![Kitty](/img/2019-01-23-the-current-state-of-macos-terminal-emulation/Kitty.png)

https://sw.kovidgoyal.net/kitty/

Kitty is an OpenGL based, GPU powered cross platform app that is as fast as it
is featureful. It focuses on keyboard based accessibility and aims to
implement (and has implemented) all of the modern functionality expected in a
terminal emulator while offering future proofing as well in the form of
extensibility. The state of the app on macOS in particular in 2019 is
outstanding and if you've not tried it recently, I highly recommend giving it a
go.

##### Pros
* Open source and cross-platform
* Offers the "Kittens" extensions system
* The only option delivering both high, transparent window based performance (Alacritty) and lots of features (iTerm2)

#### Cons
* Bright colors might appear dull if the app outputting them wrongly sends "bold" for brightness (see [issue 197](https://github.com/kovidgoyal/kitty/issues/197))


## Comparison Table

I chose 9 criteria that I considered to be important to me and placed all of
the emulators in a table to see how they compare. There are subtleties that
are not depicted here... perhaps tabs are supported in one, but not as well as
in another, or perhaps the raw performance benchmarks tell a different tale
than the three `Y` entries for GPU support do, but I feel it gives a good quick
aid in making a choice.

**Open:** Open Source  
**XP:** Cross Platform  
**GPU:** Leverages the GPU for performance  
**24:** Supports 24-bit true color  
**Wins:** Supports multiple OS application windows  
**Tabs:** Supports multiple OS application tabs  
**Splits:** Supports split window sessions within the same single OS application window  
**Img:** Renders full color images at the command line  
**.conf:** Uses a text based config file  

|| Open | XP | GPU | 24 | Wins | Tabs | Splits | Img | .conf |
| :--           | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| **Terminal**  | N   | N   | N   | N   | Y   | Y   | N   | N   | N   |
| **Alacritty** | Y   | Y   | Y   | Y   | N   | N   | N   | N   | Y   |
| **Hyper**     | Y   | Y   | N   | N   | Y   | Y   | Y   | N   | Y   |
| **iTerm2**    | Y   | N   | Y   | Y   | Y   | Y   | Y   | Y   | N   |
| **Kitty**     | Y   | Y   | Y   | Y   | Y   | Y   | Y   | Y   | Y   |


## Conclusion

I've used all five of these macOS terminal emulators extensively and selected
each to be on this list because they are my favorites. While the pros and cons
lists and comparison table represent my personal take on each, in the end I
would be and have been quite happy using any of these selections.

For corrections, opinions, or just to say hey - get at me using one of the
links below.
