---
title: "Kurosawa, Bergman, and Tessai"
date: 2019-09-07T02:06:45-0700
draft: false
tags: ["film", "art", "wisdom"]
categories: ["film"]
author: "james bunch"
comment: false
toc: false
---

When Ingmar Bergman (director, The Seventh Seal - one of my top ten films) turned 70 he was sent a letter by Akira Kurosawa (director, Seven Samurai - another top ten favorite).

> Dear Mr. Bergman,
> 
> Please let me congratulate you upon your seventieth birthday.
> 
> Your work deeply touches my heart every time I see it and I have learned a lot from your works and have been encouraged by them. I would like you to stay in good health to create more wonderful movies for us.
> 
> In Japan, there was a great artist called Tessai Tomioka who lived in the Meiji Era (the late 19th century). This artist painted many excellent pictures while he was still young, and when he reached the age of eighty, he suddenly started painting pictures which were much superior to the previous ones, as if he were in magnificent bloom. Every time I see his paintings, I fully realize that a human is not really capable of creating really good works until he reaches eighty.
> 
> A human is born a baby, becomes a boy, goes through youth, the prime of life and finally returns to being a baby before he closes his life. This is, in my opinion, the most ideal way of life.
> 
> I believe you would agree that a human becomes capable of producing pure works, without any restrictions, in the days of his second babyhood.
> 
> I am now seventy-seven (77) years old and am convinced that my real work is just beginning.
> 
> Let us hold out together for the sake of movies.
> 
> With the warmest regards,
> 
> Akira Kurosawa

It's a beautiful letter. It's especially beautiful that Kurosawa tells Bergman that he'd learned from him.

The Japanese master artist Tessai who Kurosawa references didn't suddenly become capable of producing even more stunning work at the age of 80 simply by surviving that long. Keep in mind that Tessai was already considered the best and appointed at age 70 as the official painter to the emperor of Japan.

Instead, Tessai was able to improve for two reasons; he kept learning, and he kept practicing.

Tessai was a lifelong scholar and also ended up guiding other scholars as he became a professor. He drew inspiration from his contemporaries while always retaining his own unique style. As for practicing, Tessai was extremely prolific, producing an estimated 20,000 works throughout his lifetime and at one point 70 in a single day.

We would all do well to remember to keep learning and keep producing while looking after ourselves and our longevity. Wherever we are at on the road toward or beyond 80, let's you and I, dear reader, encourage each other as Kurosawa encouraged Bergman.

So what became of Bergman and Kurosawa once they reached 80 years of age? Well, both continued to produce new works. Whether these works made for a sharp uptick in quality as Tessai's did is a determination I'll leave up to the reader. But they certainly kept learning and creating while inspiring their fans to do the same.

<center>
![Abe-no-Nakamaro Writing Poem While Moon-viewing by Tomioka Tessai 1918](/img/2019-09-07-kurosawa--bergman--and-tessai/abe-no.png)<br />
_Abe-no-Nakamaro Writing Poem While Moon-viewing by Tomioka Tessai 1918_
</center>
