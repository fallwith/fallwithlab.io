---
title: "Trader Joe's Takis"
date: 2019-02-11T19:24:14-08:00
draft: false
tags: ["food"]
categories: ["personal"]
author: "james bunch"
comment: false
toc: false
---

![takis](/img/2019-02-11-trader-joes-takis/tjstakis.jpg)


Years ago I worked at a tech startup at the beach in a wing resembling a greenhouse in a building that looked like [Jorrvaskr](https://www.google.com/search?q=Jorrvaskr&source=lnms&tbm=isch) from Skyrim.

Great, lasting friendships came out of that greenhouse, as we weathered the ups and downs of startup life shoulder to shoulder.

During one of the down turns, management decided to try to save money by putting an end to providing us with free snacks.

In response, the greenhouse gang started chipping money into a common pool to fund our own snack purchases. Our favorites? Takis.

--  

When my beautiful wife and I were dating, she'd come down to visit me in San Diego and before she eventually got her bearings as to where everything was, she'd always ask me "is there a Trader Joe's nearby?" whenever we were out and about.

So when it came time to purchase a house and our real estate agent found one that looked to be right next door to Trader Joe's on an aerial view map, I knew it was the one.

"Here you go love, there's your Trader Joe's whenever you're wanting it."

--  

Of course we shop Trader Joe's quite frequently now and we're always keen to see which new products are on the go.

And this month Trader Joe's has introduced their very own chili lime flavored rolled corn tortilla chips; their very own Takis!

--  

My Takis world and my Trader Joe's world have collided and the result is so smashingly wonderful that I consider Trader Joe's take on Takis to be even better than the real thing.

Give them a try: [Trader Joe's chili lime flavored corn tortilla chips](https://www.traderjoes.com/digin/post/chili-lime-flavored-rolled-corn-tortilla-chips)

