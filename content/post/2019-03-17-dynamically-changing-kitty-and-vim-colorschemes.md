---
title: "Dynamically Changing Kitty and Vim Colorschemes"
date: 2019-03-17T23:05:14-07:00
draft: false
tags: ["tech", "unix", "vim", "terminal", "kitty"]
categories: ["tech"]
author: "james bunch"
comment: false
toc: false
---

{{< youtube JnxFtonWzvc >}}

Much of my life is spent on a computer and most of that time is spent inside a
terminal emulator. With the advancement of the incredible
[libvterm](https://launchpad.net/libvterm) library, I may one day spend time
inside of a graphical Vim or Emacs editor instead, but for now I use
[Neovim](https://neovim.io/) inside of
[Kitty](https://sw.kovidgoyal.net/kitty/).

My reasons for using Kitty have been [mentioned previously](../2019-01-23-the-current-state-of-macos-terminal-emulation/). My reasons for using Neovim might one day warrant a blog post of their own
but for now suffice it say:

* I prefer Vim/Neovim to Emacs when within a terminal emulator
* GUI Emacs doesn't yet have great stable support for libvterm or the like
  in order to allow me to continue to use a single app window
* I prefer Neovim's handling of terminal emulation to Vim's (despite them both
  leveraging libvterm)

With Kitty (as with Alacritty), you lose window transparency if you allow a
terminal app to specify a background color. For Vim, I work around this issue
be setting the background color to `NONE` after loading my colorscheme:

```
" ~/.vimrc
" allow alacritty/kitty to retain transparency with (n)vim
" https://github.com/jwilm/alacritty/issues/1082
highlight Normal ctermbg=NONE guibg=NONE
```

Unfortunately, this workaround means that I have to update my Vim colorscheme
and my Kitty colorscheme in lockstep.

As shown in the video above, I put together three simple shell functions to
change Kitty and Vim at the same time:

* `light` - for a white background light scheme
* `yellow` - for a yellow background light scheme
* `dark` - for a grey/black dark scheme

Here's how it all works...

First off, Kitty must be configured to support remote control so that an
already running instance of Kitty can be remotely altered on the fly. For
this, set `allow_remote_control` to `yes`:

```
# ~/.config/kitty/kitty.conf
allow_remote_control yes
```

Next, place a few dedicated colorscheme related config files in the same
directory with `kitty.conf`.

For the video in this post, I placed `gruvbox-dark.conf` ("dark"),
`gruvbox-light.conf` ("yellow"),
and `seagull.conf` ("light") in my `~/.config/kitty` directory. You can find
the contents for these in my
[Kitty terminal emulator colorschemes](https://gist.github.com/fallwith/a1cec05d1664adbaa3421908472c51ca)
gist.

Next create a `colorscheme.conf` symlink pointing at your desired starting
colorscheme:

```
cd ~/.config/kitty
ln -s seagull.conf colorscheme.conf
```

Now have Kitty source this dedicated colorscheme config file symlink.
NOTE that you'll want to either comment out all color related settings in
`kitty.conf` or place your `include` line after all color related configuration
occurs:

```
# ~/.config/kitty/kitty.conf
include colorscheme.conf
```

On the Vim side, you'll want to have three corresponding colorschemes
ready to go. For the video in this post, I used
[gruvbox](https://github.com/morhetz/gruvbox) with a dark background ("dark"),
gruvbox again with a light background ("yellow"), and 
[seabird's seagull](https://github.com/nightsense/seabird) ("light"). With
these colorschemes in place, my `~/.vimrc` contains the following:

```
" ~/.vimrc
if filereadable($HOME.'/.config/kitty/vimcolorscheme')
  source ~/.config/kitty/vimcolorscheme
else
  set background=dark
  colorscheme gruvbox
end
" allow alacritty/kitty to retain transparency with (n)vim
" https://github.com/jwilm/alacritty/issues/1082
highlight Normal ctermbg=NONE guibg=NONE
```

Now Vim will look for a colorscheme related config file to exist in the
Kitty config directory and use it if found. Otherwise it will fall back to
a default colorscheme to use.

Now we have Kitty and Vim both set to look for dedicated colorscheme
configuration, it's time to bring everything together by definining the
`dark`, `yellow`, and `light` shell functions used in the video:

```
# ~/.mkshrc or ~/.bashrc etc.
function setcolorscheme() {
  vimcontent="$1"
  kittycolors="$2"
  vcs=~/.config/kitty/vimcolorscheme
  rm -f $vcs
  echo -e "$vimcontent" > $vcs
  ln -sf "$HOME/.config/kitty/$kittycolors" "$HOME/.config/kitty/colorscheme.conf"
  kitty @ set-colors --all --configured "~/.config/kitty/$kittycolors"
}
function dark() {
  setcolorscheme "set bg=dark\ncolorscheme gruvbox" gruvbox-dark.conf
}
function yellow() {
  setcolorscheme "set bg=light\ncolorscheme gruvbox" gruvbox-light.conf
}
function light() {
  setcolorscheme "set bg=light\ncolorscheme seagull" seagull.conf
}
```

Now with the `dark`, `yellow`, and `light` functions, the colorschemes for
Kitty and Vim are updated in tandem. In future, I may tie this functionality
to time-of-day related triggers to use each colorscheme automatically at
different times during the day.

If you have suggestions for improving upon this approach (perhaps ideas for
updating Vim hot instead of requiring it to be restarted) or any feedback,
please drop me a line via any of the platforms linked below.
