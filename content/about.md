---
title: "About"
date: 2019-01-23T11:38:01-08:00
lastmod: 2019-01-23T11:38:01-08:00
menu: "main"
weight: 50
---

### The author

Hi, I'm James (Fallwith).

I'm into [tech](https://www.raspberrypi.org),
[software development](https://www.ruby-lang.org),
[motorcycles](https://www.triumphmotorcycles.com),
[film](https://www.criterion.com),
[gaming](https://www.nintendo.com),
[reading](https://www.shambhala.com/the-compass-of-zen-419.html),
and the [beach](https://www.parks.ca.gov/?page_id=658).

So much of my creative output is kept relatively private. At the prodding
of friends, this blog was launched in the hopes of changing that.

### "Akira's Fishing"?

When I’m asked what my favorite film is, I usually rotate between a few top
choices to answer with. One of these is Akira Kurosawa’s 1954 masterpiece,
_Seven Samurai_.

From the _Seven Samurai_ [wiki page](https://en.wikipedia.org/wiki/Seven_Samurai):

> The film took a year to complete. It had become a topic of wide discussion long before it was released. After three months of pre-production the film had 148 shooting days spread out over a year—four times the span covered in the original budget, which eventually came to almost half a million dollars. Toho Studios closed down production at least twice. Each time, Kurosawa calmly went fishing, reasoning that the studio had already heavily invested in the production and would allow him to complete the picture.

### The back end

This blog is served up via [GitLab Pages](https://about.gitlab.com/product/pages/).

GitLab Pages supports a large number of content generator projects. See
GitLab's [examples](https://gitlab.com/pages) for the full list of what's
supported.

I chose [Hugo](https://github.com/gohugoio) with the
[Jane](https://github.com/xianmin/hugo-theme-jane) theme.

This blog's git repository is available at: [https://gitlab.com/fallwith/fallwith.gitlab.io](https://gitlab.com/fallwith/fallwith.gitlab.io)

To try out Hugo:

Search through the available [Hugo themes](https://themes.gohugo.io), select
one you like, and obtain the git URL for it from the theme's homepage. Then
follow these instructions:

```
brew install hugo
hugo new site <directory name>
cd <directory name>
git checkout <hugo theme repo url> themes/<theme name>
hugo new post/YYYY-MM-DD-the-post-title.md
vi config.toml                                # set the theme to <theme name>
vi content/post/YYYY-MM-DD-the-post-title.md  # write some content
hugo server -D
# go to localhost:1313
```

To host your own GitLab Pages site with Hugo (assuming you already
have a site created and tested locally using the instructions above):

1. Create a new GitLab project, named `<username>.gitlab.io`
2. Copy my [.gitlab-ci.yml](https://gitlab.com/fallwith/fallwith.gitlab.io/blob/master/.gitlab-ci.yml)
   file to the root of the Hugo site directory
3. Prep the Hugo site directory to serve as a git repo clone:

```
cd <directory name>
echo "<username>'s Blog" > README.md
git init
git remote add origin git@gitlab.com:<username>/<username>.gitlab.io.git
git submodule add <hugo theme repo url> themes/<theme name>
git add .
git push -u origin master
# go to https://<username>.gitlab.io
```
